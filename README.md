# SAVAGE-benchmarks

### Contents ###

This repository contains the simulated benchmarking data sets used for the SAVAGE paper, as well as all config files and scripts necessary for the benchmarking experiments. 
SAVAGE is a tool for viral quasispecies assembly, its source code is publicly available [here](http://bitbucket.org/jbaaijens/savage). The details of all experiments are described in the [preprint](http://biorxiv.org/content/early/2016/10/12/080341) available on bioRxiv.

Update (June 5th, 2019): adding new benchmarks (poliovirus and length-vs-count experiment) corresponding to the [vg-flow preprint](https://www.biorxiv.org/content/10.1101/645721v2).

Update (December 11th, 2019): adding ground truth fasta file for 15-strain-ZIKV data set (see [Downloads](https://bitbucket.org/jbaaijens/savage-benchmarks/downloads/)).