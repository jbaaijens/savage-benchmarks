#! /bin/bash -x

#
# Script for running all SGA subprocesses until contig assembly, all with default
# parameter settings. The only parameter this script takes is the algorithm used 
# for error correction.
#

# We assume the data is downloaded from the SRA and converted to fastq files
# Set IN1 and IN2 to be the paths to the data on your filesystem
#IN1=/export/scratch2/structvar/viralquasispecies/data/5VM_sim_20000x/forward.fastq
#IN2=/export/scratch2/structvar/viralquasispecies/data/5VM_sim_20000x/reverse.fastq
IN1=$1 # pass fastq files as arguments on the command line
IN2=$2
ALGO=$3

# Parameters
SGA_BIN=sga

# The number of threads to use
CPU=16

# First, preprocess the data to remove ambiguous basecalls
$SGA_BIN preprocess --pe-mode 1 -o preprocessed.fastq $IN1 $IN2

#
# Error correction
#
# Build the index that will be used for error correction
# As the error corrector does not require the reverse BWT, suppress
# construction of the reversed index
$SGA_BIN index -a ropebwt -t $CPU --no-reverse preprocessed.fastq

# Perform error correction using overlaps
$SGA_BIN correct --algorithm=$ALGO -t $CPU -o reads.ec.default.fastq preprocessed.fastq;

# Index the corrected data.
$SGA_BIN index -a ropebwt -t $CPU reads.ec.default.fastq;

# Remove exact-match duplicates and reads with low-frequency k-mers
$SGA_BIN filter -t $CPU reads.ec.default.fastq;

# Merge simple, unbranched chains of vertices
$SGA_BIN fm-merge -t $CPU -o merged.default.fa reads.ec.default.filter.pass.fa;

# Build an index of the merged sequences
$SGA_BIN index -d 1000000 -t $CPU merged.default.fa;

# Remove any substrings that were generated from the merge process
$SGA_BIN rmdup -t $CPU merged.default.fa;

# Compute the structure of the string graph
$SGA_BIN overlap -t $CPU merged.default.rmdup.fa;

# Perform the contig assembly without bubble popping
$SGA_BIN assemble -o assemble.default merged.default.rmdup.asqg.gz;
